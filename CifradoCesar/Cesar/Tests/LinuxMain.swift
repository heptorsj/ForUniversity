import XCTest
@testable import CesarTests

XCTMain([
    testCase(CesarTests.allTests),
])
