# Equipo:
# 1.- Jacales Rojas Héctor Daniel
# 2.- Ocaña Paredes Kidcia Mireya
# 3.- Daniel Alberto Hernández Guevara
# 4.- Zuno Sánchez Ricardo
#------------------------------------------------------------------------------
# Programa que encripta un mensaje acorde al Cifrado César
# https://es.wikipedia.org/wiki/Cifrado_C%C3%A9sar
# Algoritmo de decifrado, recibe el texto cifrado y el número de recorridos
# que se usarón para su cifrado.

# Algoritmo de descifrado, recibe una cadena de texo y el número de recorridos
# con que fue cifrado el texto originalmente.
# [String,Int]
def cesarDesc(texto,recorrido)
  cesar = "" # Mensaje que regresa el algoritmo, se inicializa vacio
  if recorrido > 52 # Si el recorrido es mayor a 52 se extrae el modulo para
    # saber cuantos ciclos realiza el decifrado.
    recorrido = recorrido % 52
  else # En caso contrarío se realiza un decifrado normal.
    recorrido = recorrido
  end
  r = -1 # El descifrado comienza decrementando el primer caractér y de ahí
  # alterna entre incremento y decremento.
  texto.split("").each do |c| # Separamos cada caracter del texto
    recorrido.times do # Recorremos hacia atras n veces el texto, n es indicado
      # por el parametro recorrido
      if (c.ord + r) < 65 # Si se sobrepasa el valor permitido por el codigo de
        # colores ASCII regresamos al último "z".
        c = 'z'
      elsif (c.ord + r) < 97 && (c.ord + r) > 90 && r > 0 # Si se alcanza un valor
        # invalido, después de 'Z' se recorre el caractér a 'a'
        c = 'a'
      elsif (c.ord + r) < 97 && (c.ord + r ) > 90 && r < 0 # Si se alcanza un valor
        # invalido antes de 'a' se recorre el caractér a 'Z'
        c = 'Z'
      elsif (c.ord + r ) > 122 # Si se sobrepasa el último valor valido en ASCII
        # el caractér adquiere el valor de 'A', primer valor valído.
        c = 'A'
      else
        c = (c.ord + r ).chr # Se hace el decremento o incremento segun corresponda
      end
    end
    cesar = cesar + c # Se concatena el caractér descifrado
    r = r * -1 # Se alternada
    # incremento => decremento
    # decremento => incremento
  end
  return cesar
end
# El método cesarEncr recibé dos parametros, el texto a cifrar y el número de
# recorridos que cada caracter va a ser dezplazado, de forma alternada.
# [String, Int]
def cesarEncr(texto,recorrido)
  cesar = "" # Variable que regresa la función, se inicializa como una cadena
  # de texto vacía.
  # Optimización de cifrado cuando el usuario quiere hacer mas de 52 recorridos
  if recorrido > 52 # Si el recorrido es mayor a 52 se extrae el modulo para
    # saber cuantos ciclos realiza el cifrado.
    recorrido = recorrido % 52
  else # En caso contrarío se realiza un cifrado normal.
    recorrido = recorrido
  end
  r = 1 # Variable de incremento o decremento de la cadena. Para el cifrado empieza
  # incrementando
  texto.split("").each do |c| # La cadena de texto se maneja como una Lista.
    # iterando sobre cada caractér del texto
    if c.eql? " "
      #El cifrado ignora los espacios en blanco
    else
      recorrido.times do # Se hace un ciclo delimitado por el numero de recorridos
        if (c.ord + r) < 65 # Si el caracter decrece mas haya de 'A', el nuevo
         # ciclo empezara a partir de 'z' el último caracter ASCII valido
          c = 'z'
        elsif (c.ord + r) < 97 && (c.ord + r ) > 90 && r < 0 # Si el caractér
          # decrementa mas haya de 90 el siguiente valor ASCII valido es 'Z'
          c = 'Z'
        elsif (c.ord + r) < 97 && (c.ord + r ) > 90 && r > 0 # Si el caractér
          # se incrementa mas haya de 97 el carácter del siguiente ciclo
          # comenzara en 'a'
          c = 'a'
        elsif (c.ord + r ) > 122 # Cuando se sobrepasa el valor ASCII de letras
          # el caracter adquiere el valor de 'A' el primer valor ASCII del
          # alfabeto
          c = 'A'
        else
          c = (c.ord + r).chr # cuando se encuentra en un intervalo valído
          # el incremento o decremento se realiza de forma ordinaria.
        end
      end
      cesar = cesar + c # Cuando se encuentra el caractér correspondiente
      # se concatena con la cadena formada
      r = r * -1 # Se invierte el valor de r.
      # incremento => decremento
      # decremento => incremento
    end
  end
  return cesar # el método devuelve una cadena de texto cifrada
end
#-------------------------------------------------------------------------------
# Parte operacional del programa, método que hace llamadas de cifrad o descifrado
# segun corresponda a la orden del usuario.
def menu()
  puts "Selecciona Una Opción: 1.- Cifrar 2.- Descifrar 3.- Salir"
  opcion = gets.chomp.to_i
  #mensajes = Array.new()
  case opcion # Parte del programa que redirije a la funcion de cifrado o
    # descifrado según corresponda.
  when 1
    puts "Ingresa Mensaje A Cifrar: "
    original = gets.chomp
    puts "Ingresa el número de posiciones a recorrer: "
    rec = gets.chomp.to_i
    puts "Cifrando....."
    cifrado = cesarEncr(original,rec)
    puts cifrado # Muestra el mensaje cifrado
    menu() # regresa el control al menú
  when 2
    puts "Ingresa mensaje a descifrar"
    encr = gets.chomp
    puts "Ingresa los recorridos del cifrado"
    rec = gets.chomp.to_i
    puts "Descifrando"
    descifrado = cesarDesc(encr,rec)
    puts descifrado # Muestra el mensaje descifrado
    menu()
  when 3
    return 0
  else
    puts "Opcion Incorrecta"
    menu() # regresa el control al menú
  end
end
menu
