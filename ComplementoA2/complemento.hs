import Data.Char  (digitToInt,intToDigit) -- para representar cáracteres con numeros
import Numeric    (readInt,showHex, showIntAtBase) -- para representar números en
-- base dos y leer enteros desde la entrada estandar.
-- Equipo --
-- 1.- Jacales Rojas Héctor Daniel
-- 2.- Ocaña Paredes Kidcia Mireya
-- 3.- Daniel Alberto Hernández Guevara
-- 4.- Zuno Sánchez Ricardo
-- Programa que cálcula el complemento a 1 y 2 de un número entero.
-- Inicia ejecución del programa
-- Funcion que convierte un número decimal a Binario
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Funciones para convertir de Binario a Decimal
-- Función que determina cuantos bits se usaron para codificar un número
parsebits :: [Char] -> [Char] -> Int
parsebits b n = do
  if length b == 0 -- si el usuario quiere que el ultimo bit sea el bit de signo
  then length n
  else (read b :: Int) -- soporta casos en los que el usuario indica el numero de bits
-- con los que se represento el numero
invBits :: Char -> Char -- Función que invierte Bits, usada para el complemento a 1
-- para tipos Char
invBits '0' = '1'
invBits '1' = '0'
-- Función que obtiene la representación binaria de un número entero, positivo
invBit :: Int -> Int -- Función que invierte Bits, usada para el complemento a 1
invBit 0 = 1
invBit 1 = 0
--------------------------------------------------------------------------------
-- Función que convierte una binario almacenado como cadena de texto a Entero
bin2dec :: String -> Integer
bin2dec = foldr (\c s -> s * 2 + c) 0 . reverse . map c2i
    where c2i c = if c == '0' then 0 else 1
--------------------------------------------------------------------------------
-- Función que muestra un número binario como una lista de Enteros [Int]
mostrarBinario :: [Int] -> Int -> [Int]
mostrarBinario bnum bits = do
  if (length bnum) > 0
    then [0| x <- [0.. bits - (length bnum) - 1]] ++ bnum
    else error "Bits insuficientes para representar numero"
-- Función que obtiene la representación en binario de cuálquier numero entero.
decToBin x = reverse $ decToBin' x
  where
    decToBin' 0 = []
    decToBin' y = let (a,b) = quotRem y 2 in [b] ++ decToBin' a
--------------------------------------------------------------------------------
-- Función que obtiene el complemento a 1 de cualquier numero
comp1 :: Int -> [Int] -> Int -> [Int]
comp1  num bnum bits | num == 0 = [0| x <- [0..bits-1]] -- en caso del 0 solo
-- se ponen ceros, aunque poner unos tambien es valido
         | num < 0 = do -- en caso de ser un número negativo
         -- si contamos con los bits necesarios, bits para representar + bit de
         -- signo procedemos a invertir los valores con la funcion invBit
           if bits - (length bnum) > 0
             then [1| x <- [0.. (bits -  (length bnum) - 1)]] ++ (map invBit bnum)
             else error "Bits insuficientes para representar numero"
         | num > 0 = mostrarBinario bnum bits -- en caso de ser un número positivo
         -- simplemente lo representamos como tal y añadmimos los ceros
         -- a la derecha correspondientes
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Función que obtiene el complemento a 2 de un número
comp2 :: Int-> [Int] -> Int -> [Int]
comp2 num bnum bits | num == 0 = [0| x <- [0..bits-1]]
                    | num < 0 = do
                      if bits - (length bnum) > 0
                        then [1| x <- [0.. (bits - (length bnum) - 1)]] ++ [0| x <- [0..(bits - ( length (decToBin ((2 ^ (length bnum )) - abs num)) + (bits - (length bnum)+1)) )]] ++ decToBin ((2 ^ (length bnum )) - abs num)
                        else error "Bits insuficientes para representar numero"
                    | num > 0 = mostrarBinario bnum bits

--------------------------------------------------------------------------------
-- Funcion que obtiene el valor decimal de un número representadoe en complemento a 1
cunotoDec :: [Char] -> Int -> Integer
cunotoDec num bits | bits <= length num = do
  let signo = num !! ((length num) - bits)
  let (h,t) = splitAt (length num - (bits - 1)) num
  if signo == '1'
    then    -(bin2dec (map  invBits t))
    else   (bin2dec t)
 | bits > length num = error "Demasiados bits"
--------------------------------------------------------------------------------
-- Función que obtiene el válor decimal de un número representado en complemento a 2
cdostoDec :: [Char] -> Int -> Integer
cdostoDec num bits | bits <= length num = do
  let signo = num !! ((length num) - bits)
  let (h,t) = splitAt (length num - (bits - 1)) num
  if signo == '1'
    then - ((bin2dec (map  invBits t)) + 1)
    else (bin2dec t)
 | bits > length num = error "Demasiados bits"

-- Menu recibe un valor entero y regresa una llamada a IO
menu :: Int -> IO()  -- Redirije a la funcion correcta
-- "Pattern Maching" de la función
menu 1 = do -- Control del programa que representa un numero en su correspondiente
-- valor en complemento a 1 y 2
  print "Ingresa Numero A Representar En Complemento: "
  n <- getLine
  let num = (read n :: Int)
  print "Ingresa Numero De Bits Para Representar: "
  b <- getLine
  let bits = (read b :: Int)
  let c = map abs (decToBin num)
  let c1 = comp1 num c bits
  putStr "Binario [ Valor Absoluto ] = "
  print $ showIntAtBase 2 intToDigit (abs num) ""
  putStr "Complemento a 1 = "
  print $ c1
  let c2 = comp2 num c bits
  putStr "Complemento a 2 = "
  print $ c2
menu 2 = do -- Control del programa que  a partir de un numero binario
-- obtiene su valor decimal.
  print "Ingresa Numero Binario"
  bin <- getLine
  print "Ingresa Numero de Bits Para Representar"
  print " si se deja vacio el ultimo bit se tomara como bit de signo"
  b <- getLine
  let bits = parsebits b bin
  let dec1 = cunotoDec bin bits
  let dec2 = cdostoDec bin bits
  putStr "En Binario = "
  let a = bin2dec bin
  print $ a
  putStr "Valor Decimal del Complemento a 1 = "
  print $ dec1
  putStr "Valor Decimal del Complemento a 2 = "
  print $ dec2
  print bin
-- Control de ejecución del programa
main :: IO ()
-- La aplicación permite obtener el codigo en complemento a 1 y 2 de un número
-- y tambien permite la decodificación de complmento a su valor dedimal.
main = do
   print "Representacion en Complemento [1 y 2 ] de numeros decimales y viceversa"
   print "Selecciona Una Opcion: 1- Decimal A Complemento 2- Complemento [1 y 2 ] A Decimal"
   s <- getLine
   let opcion = (read s::Int)
   menu opcion
