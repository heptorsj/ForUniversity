# Archivo auxiliar de operaciones
class Operaciones
  def initialize # Este constructor contiene variables globales para sus métodos.
     @resultadoVector = Array.new
     @resultadoEscalar = 0
  end
  def calcAngulo(vectorA,vectorB) # Método que cálcula el ángulo de dos vectores.
    i = 0
    numerador = 0
    # El numerador es el producto punto de ambos vectores.
    vectorA.each do |elemento|
      numerador += (elemento * vectorB[i])
      i += 1
    end
    denominador = self.norma(vectorA)*self.norma(vectorB) # El denominador es el producto de las normas.
    angulo = Math.acos(numerador/denominador) # Con auxilio de la Libreria Math podemos cálcular el arco coseno del angulo.
    @resultadoEscalar = (angulo * 360) / (Math::PI * 2) # Ruby devuelve el resultado en radianes por lo que hay que convertirlo a grados.
    return @resultadoEscalar
  end
  def suma(vectorA,vectorB) # Método que suma dos vectores.
    aux = Array.new
    i = 0
    vectorA.each do |elemento| # La suma se realiza componente a componente.
      aux.push(elemento + vectorB[i]) # Se añade el nuevo elemento en el vector auxiliar.
      i += 1
    end
    @resultadoVector = aux # El método devuelve el nuevo vector.
    return @resultadoVector
  end
  def resta(vectorA,vectorB) # Método que resta dos vectores, misma estructura que la suma.
    aux = Array.new
    i = 0
    vectorA.each do |elemento|
      aux.push(elemento - vectorB[i]) # Se cambia el operador + por el -
      i += 1
    end
    @resultadoVector = aux # De la misma forma devuelve el nuevo vector.
    return @resultadoVector
  end
  def productoEscalar(vector,escalar) # Método que realiza el producto escalar, recibe el vector y el factor a multiplicar.
    tmp = Array.new
    vector.each do |elemento|
      tmp.push(elemento*escalar) # Multiplicamos cada componente por el escalar y lo guardamos en un vector temporal
    end
    @resultadoVector = tmp
  end
  def norma(vector) # Método que cálcula la norma de cualquier vector
    tmp = 0
    vector.each do |elemento|
      tmp += (elemento*elemento) # En una variable temporal guardamos la suma de los cuadrados de los componentes.
    end
    @resultadoEscalar = Math.sqrt(tmp) # por ultimo obtenemos la raiz cuadrada de esa suma.
    return @resultadoEscalar
  end
end
