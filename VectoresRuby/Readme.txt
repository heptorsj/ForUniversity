#-------------------------------------------------------
 MIEMBROS DEL EQUIPO:
 1.- Jacales Rojas Héctor Daniel
 2.- Ocaña Paredes Kidcia Mireya
 3.- Daniel Alberto Hernández Guevara
 4.- Zuno Sánchez Ricardo

#-------------------------------------------------------

Programa que realiza operaciones sobre vectores.

Para ejecutar el programa se necesita Instalar Ruby 2.3 o Súperior.
En la terminal ejecutar el archivo main.rb
 
 ruby main.rb

El menú desplegara las opciones que se pueden hacer con los vectores,
así como crear los mismos.
