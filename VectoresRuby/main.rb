# encoding: UTF-8
# Menú de operaiones sobre 2 vectores de n dimenciones.
# Equipo:
# MIEMBROS DEL EQUIPO:
# 1.- Jacales Rojas Héctor Daniel
# 2.- Ocaña Paredes Kidcia Mireya
# 3.- Daniel Alberto Hernández Guevara
# 4.- Zuno Sánchez Ricardo

#
require "./Operaciones.rb" # Libreria de métodos auxiliar, en ella se encuentran implementadas las operaciones vectoriales.
#-------------------------------------------------------------------------------------------------#
# Esta clase contiene la funcionalidad del programa, menú y llamadas a métodos
# de las operaciones con vectores.
class Programa
    def initialize # Método constructor de la clase
      @vectores = Hash.new # Esta variable de instancia guarda los vectores y les asigna un índice. Formato: [índice => vector]
      @calculadora = Operaciones.new # Implementación de la calculadora de operaciones con vectores.
    end
    def Menu # Menú que da funcionalidad al programa.
        puts "Opciones: [1] Crear Vectores [2] Suma [3] Resta [4] Producto Escalar [5] Norma [6] Ángulo Entre Vectores [7 Salir]"
        opcion = gets.chomp.to_i
        case opcion
        when 1
          crearVectores # Llamada a la funcion local, que permite crear y guardar vectores
          self.Menu # Este tipo de llamada self.Menu devuelve el control del programa al Menu
        when 2
          if @vectores.length < 2 # Condición que verifica que existen al menos dos vectores para sumar.
            puts "No tiene suficientes vectores para realizar esta operación."
            self.Menu
          end
          puts "Usted dispone de los siguientes vectores: #{@vectores}" # Se despliegan los vectores en memoria para poder seleccionar.
          puts "Seleccione el vector A: "
          a = gets.chomp.to_i
          puts "Seleccione el vector B: "
          b = gets.chomp.to_i
          if @vectores[a].length != @vectores[b].length # Condicion que verifica que existe igualdad de cardinalidad entre vectores.
            puts "Error, Imposible realizar operación, los vectores deben tener la misma cantidad de elementos"
            self.Menu
          end
          resultado = @calculadora.suma(@vectores[a],@vectores[b])  # Esta llamada al método de la clase Operaciones hace la suma.
          puts " #{@vectores[a]} + #{@vectores[b]}  = #{resultado}"
          print "Desea guardar el nuevo vector [S/N] : " # El programa da la posibilidad de poder alamacenar el nuevo vector.
          respuesta = gets.chomp
          if respuesta == "S" || respuesta == "s"
            @vectores[@vectores.length+1] = resultado
          elsif respuesta == "N" || respuesta == "n"
          else
            puts "Error!"
          end
          self.Menu
        when 3 # La implementacion de la resta es basicamente la misma, excepto en la llamada al método de la clase Operaciones.
          if @vectores.length < 2
            puts "No tiene suficientes vectores para realizar esta operacion."
            self.Menu
          end
          puts "Usted dispone de los siguientes vectores: #{@vectores}"
          puts "Seleccione el vector A: "
          a = gets.chomp.to_i
          puts "Selecciones el vector B: "
          b = gets.chomp.to_i
          if @vectores[a].length != @vectores[b].length
            puts "Error, Imposible realizar operación, los vectores deben tener la misma cantidad de elementos."
            self.Menu
          end
          resultado = @calculadora.resta(@vectores[a],@vectores[b]) # Aquí se llama al método resta, lo que la diferencía de la suma.
          puts " #{@vectores[a]} - #{@vectores[b]}  = #{resultado}"
          print "Desea guardar el nuevo vector [S/N] : " # De igual manera el programa permite guardar el nuevo vector.
          respuesta = gets.chomp
          if respuesta == "S" || respuesta == "s"
            @vectores[@vectores.length+1] = resultado
          elsif respuesta == "N" || respuesta == "n"
          else
            puts "Error!"
          end
          self.Menu
          self.Menu
        when 4
          if @vectores.length < 1 # Condición que verifica disponibilidad de vectores en memoria.
            puts "No tiene suficientes vectores para realizar esta operacion"
            self.Menu
          end
          puts "Usted dispone de los siguientes vectores: #{@vectores}"
          print "Seleccion un vector [numero entero]: "
          vector = gets.chomp.to_i
          print "Ingresa un numero escalar: "
          escalar = gets.chomp.to_f # El número leido lo almacena como un número real.
          resultado = @calculadora.productoEscalar(@vectores[vector],escalar) # Lamada a la funcion matemática.
          puts " Resultado = #{resultado}"
          print "Desea guardar el nuevo vector [S/N] : "
          respuesta = gets.chomp
          if respuesta == "S" || respuesta == "s"
            @vectores[@vectores.length+1] = resultado
          elsif respuesta == "N" || respuesta == "n"
          else
            puts "Error!"
          end
          self.Menu
        when 5 # Este "case" realizá la operación de cálculo de la norma de un vector.
          if @vectores.length < 1
            puts "No tiene suficientes vectores para realizar esta operacion"
            self.Menu
          end
          puts "Usted dispone de los siguientes vectores #{@vectores}"
          print "Seleccion un vector [numero entero]: "
          vector = gets.chomp.to_i
          resultado = @calculadora.norma(@vectores[vector])
          puts "  Norma del vector #{@vectores[vector]} = #{resultado}"
          self.Menu
        when 6 # Este "case" realizá la función del cálculo del ángulo entre dos vectores.
          if @vectores.length < 1
            puts "No tiene suficientes vectores para realizar esta operacion"
            self.Menu
          end
          puts "Usted dispone de los siguientes vectores #{@vectores}"
          puts "Seleccione el vector A: "
          a = gets.chomp.to_i
          puts "Selecciones el vector B: "
          b = gets.chomp.to_i
          if @vectores[a].length != @vectores[b].length
            puts "Error, Imposible realizar operacion, los vectores deben tener la misma cantidad de elementos"
            self.Menu
          end
          resultado = @calculadora.calcAngulo(@vectores[a],@vectores[b])
          puts " Angulo = #{resultado}"
          self.Menu

        else
          return 0
        end
    end
    def crearVectores # Método de la Clase que sirve para leer datos de entrada de un vector.
      puts "¿Cuántos Vectores Quieres Crear?"
      n = gets.chomp.to_i # Se podran crear n vectores nuevos cada que se llame al método.
      n.times do |j| # Proceso de iteración.
        vector = Array.new # Vector temporal.
        print "Ingresa Dimensión Del Vector #{j+1}: "
        dim = gets.chomp.to_i
        dim.times do |i|
          print "Ingresa el elemento: #{i+1}: "
          elemento = gets.chomp.to_f
          vector.push(elemento) # Se añade un nuevo elmento al arreglo con la función push. Simulando el comportamiento de una pila.
        end
        key = @vectores.length + 1 # La clave o indice de cada vector se actualiza conforme se van añadiendo vectores.
        @vectores[key] = vector # Añadimos elementos al Hash.
        puts "Vectores actualizados #{@vectores}"
      end
    end
end
puts "Calcúladora de Vectores KDH 1" # Mensaje de bienvenida del programa
p = Programa.new # Se crea el objeto "P" (programa)
p.Menu # Se inicia la funcionalidad del programa.
